#!/bin/bash

### dyndns_update.sh
### check if external IP has changed and update dyndns
### Neimi
### http://huraxdaxdax.de/

# Array Domain definieren
declare -A Domain

# Domains und Update-Url festlegen - Array fuellen
Domain[irgendwas.13mail.de]=https://13mail.de/cpanelwebcall/vXXXXXXXXXXXXXXXX
Domain[etwasanderes.13mail.de]=https://13mail.de/cpanelwebcall/dXXXXXXXXXXXXXXXX
Domain[nochwasanderes.huraxdaxdax.de]=https://huraxdaxdax.de/cpanelwebcall/cXXXXXXXXXXXXXXXX

# Pfade fuer Logdateien festlegen
SaveIPFile="/var/log/dyndns_update_ip.log"
LogFile="/var/log/dyndns_update.log"

# Neue IP setzen falls curl fehlschlaegt
NewIP="0.0.0.0"

# External IP holen und Leerzeichen entfernen
NewIP=$(curl 13mail.de/show_ip_addr.php | tr -d " ")

# Aktuelles Datum
DATE=`date -R`

# Pruefen ob Logdateien existieren
checkfile(){
	if [ ! -f "$SaveIPFile" ]
	then
	    touch $SaveIPFile
	    echo -e "SaveIPFile" > $SaveIPFile
	fi
	if [ ! -f "$LogFile" ]
	then
	    touch $LogFile
	    echo -e "LogFile" > $LogFile
	fi
}

checkfile

# alte IP aus SaveIPFile
for SavedIP in `cat $SaveIPFile`
do
	echo "Logdatei-IP: $SavedIP"
	echo "neue IP: $NewIP"

	if [[ "$SavedIP" == "$NewIP" ]]
	then
		echo "Gleiche IP!"
	else
		echo "IP hat gewechselt - $NewIP"
		# Neue IP speichern
		echo $NewIP > $SaveIPFile
		# Neue IP mit Datum ins Logfile schreiben
		echo "$NewIP : changed : $DATE" >> $LogFile
		# Alle Domains updaten und ins Logfile schreiben
		for c in "${!Domain[@]}"
        	do
			LogEntry="Update $c | "
			LogEntry+=$(curl ${Domain[$c]})  
			echo $LogEntry >> $LogFile
		done
	fi

done

# wg. Problemen mit leerem SaveIPFile - Neue IP auf jeden Fall speichern
echo $NewIP > $SaveIPFile

exit 0